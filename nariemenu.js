    $(function(){
    
        /**********************
        
        動かしたいメニューのブロックの設定。
        ・position:absolute;で固定。
        ・クラス名　fixを付加してください。
        
        *********************/
        
        //要素の高さをあらかじめ取得。
        var targety = $('.fix').offset().top;

        //イベントの定義と変数化。
        var ssevent = new $.Event('scrollstop');
        //遅延時間
        var delay = 100;
        //タイマー名
        var timer;
        
        //スクロール時に呼び出される関数。
        //一番最後に呼び出されたタイマーのみが有効。
        function sstrigger(){
            if(timer){
                //timerが存在すれば消す。
                clearTimeout(timer);
            }
            //最後に発生したタイマーがあれば、delayの分遅延したのちにsseventを引き起こす。つまりscrolltopイベントが呼び出される。
            timer = setTimeout(function(){
                $(window).trigger(ssevent);
            }, delay);
        }
        
        //スクロールのたびに sstriger()を呼び出す。
        $(window).on('scroll', sstrigger);
        
        //常にscrollstopイベントを監視。イベントが発生したら何かをする。
        $(window).on("scrollstop",function(){
            
            //イベントが発火したらアニメーション。。
            //scrolltopを取得
            var bsct = $(window).scrollTop();
            //移動するべき座標
            bsct += targety;
            //scrolltop➕
            //easingプラグインで動きをカスタム。
            ////http://gsgd.co.uk/sandbox/jquery/easing/
             $('.fix').animate({top:bsct}, '100', 'easeOutBack');
        });

    });
